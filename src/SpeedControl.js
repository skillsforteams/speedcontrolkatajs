'use strict';


class SpeedControl {

    construct() {

    }

    getFee(kmhToFast, location) {
        if(location =="outside") {
            if(kmhToFast >= 71) {
                return 600
            }
            if(kmhToFast >= 61 &&  kmhToFast <= 70  ) {
                return 440
            }
            if(kmhToFast >= 51 &&  kmhToFast <= 60  ) {
                return 240
            }
            if(kmhToFast >= 41 &&  kmhToFast <= 50  ) {
                return 160
            }
            if(kmhToFast >= 31 &&  kmhToFast <= 40  ) {
                return 120
            }
            if(kmhToFast >= 26 &&  kmhToFast <= 30  ) {
                return 80
            }
            if(kmhToFast >= 21 &&  kmhToFast <= 25  ) {
                return 70
            }
            if(kmhToFast >= 16 &&  kmhToFast <= 20  ) {
                return 30
            }
            if(kmhToFast >= 11 &&  kmhToFast <= 15  ) {
                return 20
            }
            if(kmhToFast >= 1 &&  kmhToFast <= 10  ) {
                return 10
            }
        }
        if(kmhToFast >= 1 &&  kmhToFast <= 10  ) {
            return 15
        }
        return 0;
    }

}

export default SpeedControl;