import SpeedControl from './SpeedControl';



test('No fee for 0 KM per hour', () => {  
  let speedControl = new SpeedControl(); 
  let fee = speedControl.getFee(0, "city");
  expect(fee).toBe(0);    
});

test('15 Eur for 6km in city', () => {
  let speedControl = new SpeedControl(); 
  let fee = speedControl.getFee(6, "city");
  expect(fee).toBe(15);  
});
