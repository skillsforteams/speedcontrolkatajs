# Windows
Recommended Setup for my JavaScript Course and exercises für Windows Systems

## Visual Studio code IDE
1 Choice - Visual Studio Code
https://code.visualstudio.com/

Test: Ausführbar unter windows


## Chrome Browser
Alternative browser is always useful Edge|Firefox| Opera|Safari
https://www.google.de/chrome/



## GIT for Windows - 
Git Client for Version control system
 - Option 64-bit Git for Windows Setup.
Download: https://git-scm.com/download/win


### Test: 
Git Bash starten
'$git --version
'git clone https://gitlab.com/skillsforteams/millidayscom.git
Expected
file millidayscom/Readme.md ist existing




## Node JS 
Execude JavaScript code and development tools on local machine

Option: Windows Installer MSI 64 bit
https://nodejs.org/en/download/
### Test
Start Git Bash
´$ node --version
´v14.17.3 (oder grösser)

Test Complete:
Test the complete development toolset, 
specially npm (node package manager) which possibly needs proxy configuration if you work behind a firewall


$mkdir /c/data/
$mkdir /c/data/kurs/
$mkdir /c/data/kurs/javascript
$cd /c/data/kurs/javascript
$git clone https://gitlab.com/skillsforteams/speedcontrolkatajs.git
Erwartete Ausgabe:Cloning into 'speedcontrolkatajs'...
remote: Enumerating objects: 41, done.
remote: Counting objects: 100% (41/41), done.
remote: Compressing objects: 100% (37/37), done.
remote: Total 41 (delta 11), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (41/41), 124.86 KiB | 4.99 MiB/s, done.
Resolving deltas: 100% (11/11), done.

$ cd speedcontrolkatajs/
$ npm install
Erwartet: ... diverse ausgaben .... 
node_modules verzeichniss existiert und ist voll
$npm test
Erwartet
Test Suites: 1 passed, 1 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        1.808 s
Ran all test suites.

---OPTIONAL
To use GIT Via SSH key - i recommend Puttygen
## Puttygen
SSH Key Generator for windows to generate ssh keys used with gitlab
putty.org
Download: https://the.earth.li/~sgtatham/putty/latest/w64/puttygen.exe
### Test:
puttygen.exe is exacuting and Windows gui is running


